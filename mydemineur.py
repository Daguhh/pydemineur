#!/usr/bin/env python3

# MIT No Attribution
#
# Copyright (c) 2020 Daguhh
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software IS
# furnished to do so.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

__author__ = "Daguhh"
__license__ = "MIT-0"

import sys
from time import sleep, time
from itertools import product, zip_longest, cycle
from random import randint
from collections import namedtuple

import tkinter as tk
from tkinter import messagebox


COLOR_SCALE = {
    "1": "#4ad507",
    "2": "#454dff",
    "3": "#c0c000",
    "4": "#c0c000",
    "5": "#ddd500",
    "6": "#fb0000",
    "7": "#fb0000",
    "8": "#fb0000",
}

LEVELS = [
    {"name": "facile", "nb_mines": 10, "size": (9, 9)},
    {"name": "moyen", "nb_mines": 25, "size": (15, 15)},
    {"name": "difficile", "nb_mines": 120, "size": (20, 30)},
]

FLAGS = cycle(("?", "#", " "))


def tic():
    global tic_time
    tic_time = time()


def toc():
    global tic_time
    return time() - tic_time


def get_cell_text(isBomb, nearby_bombs):
    """Text to display on a cell given its status"""

    if isBomb:
        return "X"  # a bomb
    elif nearby_bombs:
        return str(nearby_bombs)  # no bomb but got neighboors
    else:
        return " "  # no bomb, no neighboors


def rand_tuple(maxs, nb_tuple=1, mins=(0,), excepted=()):
    """create tuples of randint
    tuple have same size as {maxs},
    each tuple contain randoms integer generated from their respective bound values (mins and maxs)

    Parameters
    ----------
        maxs : tuple
            maximums values of item in tuple
        nb : int
            number of tuple to create
        mins : tuple
            minimums values of item in tuple
        excepted : tuple of tuples
            forbiden tuple to create
    Returns
    -------
        list of tuples
    """

    tups = []

    while nb_tuple:
        new_tup = tuple(
            randint(m, M - 1) for m, M in zip_longest(mins, maxs, fillvalue=0)
        )

        if new_tup not in tups and new_tup not in excepted:
            tups += [new_tup]
            nb_tuple -= 1

    return tups


class LevelSelectorWindow(tk.Tk):
    def __init__(self, callback):

        super().__init__()
        self.geometry("150x250")

        self.callback = callback

        self.answer_id = tk.IntVar()
        for i, lvl in enumerate(LEVELS):
            cb_lvl = tk.Radiobutton(
                self, text=lvl["name"], value=i, variable=self.answer_id
            )
            cb_lvl.pack()

        tk.Button(self, text="Valider", command=self.set_props).pack()
        tk.Button(self, text="Quitter", command=self.close_game).pack()

        self.mainloop()

    def set_props(self, *_):
        lvl_props = LEVELS[self.answer_id.get()]
        self.callback(lvl_props)
        self.destroy()

    def close_game(self, *_):
        self.destroy()
        sys.exit(1)


CellBoard = namedtuple("CellBoard", ("pos", "isBomb", "nearby_bombs", "txt"))


class Board:
    """Just contain level properties (bomb, nearby bombs...)"""

    def __init__(self, size, nb_mines):

        self.size = size
        self.nb_mines = nb_mines
        self.mines = []
        self.cells = dict()

    def walk(self):
        return product(range(self.size[0]), range(self.size[1]))

    def __getitem__(self, pos):
        return self.cells[pos]

    def generate_level(self, cell_clicked):

        self.mines = rand_tuple(
            self.size, nb_tuple=self.nb_mines, excepted=(cell_clicked,)
        )

        for pos in self.walk():

            nearby_bombs = 0
            isBomb = False

            if pos in self.mines:
                isBomb = True
            else:
                nearby_bombs = sum(
                    int(n in self.mines) for n in self.get_neighboors(pos)
                )

            txt = get_cell_text(isBomb, nearby_bombs)

            self.cells[pos] = CellBoard(pos, isBomb, nearby_bombs, txt)

    def get_neighboors(self, pos):
        """return a generator giving a coordinate neighbooring cells"""

        i, j = pos
        ns = product(
            range(max(i - 1, 0), min(i + 2, self.size[0])),
            range(max(j - 1, 0), min(j + 2, self.size[1])),
        )

        for n in ns:
            if n != pos:
                yield n


class CellView(tk.Button):
    """Cell board front with tkinter : revealed cells and mouse interaction"""

    def __init__(self, parent, pos, cell_push_cbk):
        super().__init__(parent, text=" ")

        self.pos = pos
        self.isRevealed = False  # user already has interact with cell
        self.isLocked = False  # Prevent cell interaction with "#" flag
        self.cell_push_cbk = cell_push_cbk  # callback

        self.bind("<Button-1>", self.reveal_act)
        self.bind("<Button-2>", self.add_flag)
        self.bind("<Button-3>", self.add_flag)

    def add_flag(self, *_):

        if not self.isRevealed:
            self["text"] = next(FLAGS)
        self.isLocked = self["text"] == "#"

    def reveal_act(self, *_):
        if not self.isLocked:
            self.cell_push_cbk(self.pos)
        else:
            print("Locked with flag '#'")

    def reveal(self, char):

        self["text"] = char
        if char in COLOR_SCALE:
            self["fg"] = COLOR_SCALE[char]
        self.isRevealed = True
        self["relief"] = "flat"


class View(tk.Tk):
    """Game frontend"""

    def __init__(self, board, cell_push_cbk):

        super().__init__()

        self.cells = dict()

        for pos in board.walk():
            cell = CellView(self, pos, cell_push_cbk)
            cell.grid(row=pos[0], column=pos[1])
            self.cells[pos] = cell

    def __setitem__(self, pos, char):
        self.cells[pos].reveal(char)

    def __getitem__(self, pos):
        return self.cells[pos]


class Game:
    """Connect front (View) with game datas (Board)"""

    def __init__(self):

        self.start()

    def start(self):

        LevelSelectorWindow(self.set_props)
        tic()
        self.new_game()

    def set_props(self, lvl_props):

        self.lvl_props = lvl_props

    def new_game(self):

        lvl_name = self.lvl_props["name"]
        grid_size = self.lvl_props["size"]
        nb_mines = self.lvl_props["nb_mines"]

        self.board = Board(grid_size, nb_mines)  # Game datas
        self.view = View(self.board, self.cell_push_cbk)  # User front
        self.hidden_cells = self.board.size[0] * self.board.size[1] - nb_mines

        self.view.mainloop()

    def cell_push_cbk(self, pos):
        """callback when button press"""

        if not self.board.mines:  # generate mines after first click
            self.board.generate_level(pos)

        # track unvisited cells during reveal event propagation
        self.not_scaned = list(self.board.walk())

        if self.board[pos].isBomb:
            self.lose()

        else:
            self.view[pos].reveal(self.board[pos].txt)

            self.hidden_cells -= 1
            self.testWin()

            self.propagate(pos)

    def propagate(self, pos):
        """reveal cells until neighbooring bombs"""

        for n_pos in self.board.get_neighboors(pos):

            cell_view = self.view[n_pos]
            cell_board = self.board[n_pos]

            if not n_pos in self.not_scaned or cell_view.isRevealed:
                continue

            self.not_scaned.remove(n_pos)

            if not cell_board.isBomb:
                cell_view.reveal(cell_board.txt)

                self.hidden_cells -= 1
                self.testWin()

                if not cell_board.nearby_bombs:
                    self.propagate(n_pos)

    def testWin(self):
        if self.hidden_cells == 0:
            self.endOfgame(
                "You win!!!!",
                f'You solved {self.lvl_props["name"]} level in {toc():.2f} sec',
            )

    def lose(self):

        for pos in self.board.walk():
            self.view[pos].reveal(self.board[pos].txt)

        self.endOfgame("You're a loser!!!!", "You hit a mine! Hahahahahaah!")

    def endOfgame(self, title, msg):

        messagebox.showinfo(title, msg)
        self.view.destroy()
        self.start()


if __name__ == "__main__":
    Game()
